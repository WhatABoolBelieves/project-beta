from django.shortcuts import render
from django.http import JsonResponse
from pkg_resources import require
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json

from .models import Appointment, Technician, History

# Create your views here.

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "name",
        "date",
        "tech",
        "reason",
    ]

class HistoryEncoder(ModelEncoder):
    model = History
    properties = [
        "vin",
        "name",
        "date",
        "tech",
        "reason",
    ]



@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments}, encoder=AppointmentListEncoder
        )
    else:
        content = json.loads(request.body)
        appointments = Appointment.create(**content)
        return JsonResponse(
            appointments,
            encoder=AppointmentListEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE"])
def api_detail_appointment(request, pk):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            { "appointment": appointment },
            encoder=AppointmentListEncoder,
            safe=False,
        )
    else:
        appointment = Appointment.objects.get(id=pk)
        appointment.delete()
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )


@require_http_methods(["GET"])
def api_history_appointments(request, pk):
    appointments = History.objects.filter(vin=pk)
    return JsonResponse(
        {"appointments": appointments}, encoder=HistoryEncoder
    )

@require_http_methods(["GET", "POST"])
def api_history_list_appointments(request):
    if request.method == "GET":
        appointments = History.objects.all()
        return JsonResponse(
            {"appointments": appointments}, encoder=HistoryEncoder
        )
    else:
        content = json.loads(request.body)
        appointments = History.create(**content)
        return JsonResponse(
            appointments,
            encoder=HistoryEncoder,
            safe=False,
        )

class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "emp_num",
    ]


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse(
            {"technician": technician}, encoder=TechnicianListEncoder
        )
    else:
        content = json.loads(request.body)
        technician = Technician.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False,
        )