# Generated by Django 4.0.3 on 2022-06-24 12:33

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0008_alter_tech_emp_num'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Tech',
            new_name='Technician',
        ),
    ]
