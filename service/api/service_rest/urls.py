from django.urls import path

from .views import api_detail_appointment, api_history_appointments, api_list_appointments, api_list_technicians, api_history_list_appointments
urlpatterns = [
    path('appointments/', api_list_appointments, name="api_list_appointments"),
    path("appointments/<int:pk>/", api_detail_appointment, name="api_detail_appointment"),
    path("history/<str:pk>", api_history_appointments, name="api_history_appointments"),
    path("history/", api_history_list_appointments, name="api_history_list_appointment"),
    path("technicians/", api_list_technicians, name="api_list_technicians"),

]