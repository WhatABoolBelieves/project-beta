from django.db import models

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    color = models.CharField(max_length=50, null=True)
    year = models.PositiveSmallIntegerField(null=True)
    model =  models.CharField(max_length=100, null=True)
    manufacturer = models.CharField(max_length=100, null=True)
    has_sold = models.BooleanField(default=False)

    picture_url = models.URLField(null=True)

    def __str__(self):
        return self.vin


class SalesPerson(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveIntegerField()

    def __str__(self):
        return self.name


class Customer(models.Model):
    name = models.CharField(max_length=200, null=True)
    address = models.CharField(max_length=500, null=True)
    phone = models.CharField(max_length=50, null=True)

    def __str__(self):
        return self.name


class SaleRecord(models.Model):
    price = models.FloatField(null=True)

    customer = models.ForeignKey(
        Customer,
        related_name="salerecords",
        on_delete=models.CASCADE,
        null=True,
    )

    salesperson = models.ForeignKey(
        SalesPerson,
        related_name="salerecords",
        on_delete=models.CASCADE,
        null=True,
    )

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="salerecords",
        on_delete=models.CASCADE,
        null=True,
    )