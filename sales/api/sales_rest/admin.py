from django.contrib import admin

from .models import SalesPerson, Customer, SaleRecord, AutomobileVO


@admin.register(SalesPerson)
class SalesPersonAdmin(admin.ModelAdmin):
    pass


@admin.register(Customer)
class Customer(admin.ModelAdmin):
    pass

@admin.register(SaleRecord)
class SaleRecord(admin.ModelAdmin):
    pass

@admin.register(AutomobileVO)
class AutomobileVO(admin.ModelAdmin):
    pass