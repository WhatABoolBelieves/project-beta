# CarCar

Team:

* Nellie Nelson - Sales
* Evan Wu - Service

* Person 1 - Which microservice?
* Person 2 - Which microservice?

## Design
Here is a sketch of the design:
https://excalidraw.com/#room=fd3c03235ec57e132202,KAvJa_IkM6eEShvPLpjVDw

## Service microservice

I have four models: Appointment, Automobile, Technician, and History.

The Technician model and History models are just basic models that don't play into the integration of the Inventory and Service microservice. They're there to satisfy the technician form and service history requirements.

The Automobile model contains the vin property that the poller cares about it. The incoming data from the poller is sent to the component for list of appointments (using the Appointment model) where it is matched against the listed vins of appointments in the system. A positive match results in the vip property of the Appointment model being True which means the customer for the corresponding appointment is to receive VIP service.

Essentially, the vin is the key model property that serves as an integration tool between the two microservices.

## Sales microservice

We have three bounded contexts here: Inventory, Service and Sales, as outlined in the sketch. Inventory is the aggregate root of Service and Sales; both departments need to reference vehicle VINs and other vehicle properties and models from Inventory.

Each bounded context is comprised of the models it contains. For Sales, those models will be Sales Person, Customer and Sale Record. To access each car’s information in inventory, I’ll also add an automobile value object to access the automobile model from Inventory.

In context mapping, the Sale Record will need access to the Customer, Sales Person and Automobile entities in order to change its state. For this I’ll set a foreign key property on the Sale Record model for each of these three entities. The automobile value object in models will access inventory via the poller, using the automobile API.
