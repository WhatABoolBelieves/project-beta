import React from 'react';

class SalesPersonForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      employee_number: '',
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }


  handleChange(event) {
    const newState = {};
    newState[event.target.id] = event.target.value;
    this.setState(newState);
  }

  // do I need a componentDidMount function? Not yet sure what that component would be here.

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};

    const salesPersonUrl = 'http://localhost:8090/api/salespeople/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(salesPersonUrl, fetchConfig);
    if (response.ok) {
      // const newSalesPerson = await response.json();
      // console.log(newSalesPerson);
      this.setState({
        name: '',
        employee_number: '',
      });
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new sales person.</h1>
            <form onSubmit={this.handleSubmit} id="create-salesperson-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.name} placeholder="Name of sales person" type="text" id="name" className="form-control" />
                <label htmlFor="name">Name of sales person</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.employee_number} placeholder="Employee Number" type="number" id="employee_number" className="form-control" />
                <label htmlFor="employee_number">Employee number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default SalesPersonForm;

