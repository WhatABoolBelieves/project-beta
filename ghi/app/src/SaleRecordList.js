import React from 'react';


class SaleRecordList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      salerecords: [],
    }
  };

  async getSaleRecData() {
    const url = 'http://localhost:8090/api/salerecords/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ salerecords: data.salerecords})
    }
  }

  async componentDidMount() {
    this.getSaleRecData()
  }


  render() {
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>SalesPerson Name</th>
            <th>SalesPerson Employee Number</th>
            <th>Customer Name</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {this.state.salerecords.map(salerecord => {
            return (
              <tr key={salerecord.automobile.vin}>
                <td>{ salerecord.salesperson.name }</td>
                <td>{ salerecord.salesperson.employee_number }</td>
                <td>{ salerecord.customer.name }</td>
                <td>{ salerecord.automobile.vin }</td>
                <td>{ salerecord.price }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
}

export default SaleRecordList;
