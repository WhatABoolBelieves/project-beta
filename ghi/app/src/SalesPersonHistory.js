import React from 'react';


class SalesPersonHistoryList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      salesperson: '',
      salespeople: [],
      salerecord: '',
      salerecords: [],
    }

    // this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const newState = {};
    newState[event.target.id] = event.target.value;
    this.setState(newState);
  }

  async componentDidMount() {
    const saleRecUrl = 'http://localhost:8100/api/salerecords/';
    const salespersonUrl = 'http://localhost:8090/api/salespeople/';

    const saleRecResponse = await fetch(saleRecUrl);
    const salespersonResponse = await fetch(salespersonUrl);


    if (saleRecResponse.ok && salespersonResponse.ok) {
      const saleRecData = await saleRecResponse.json();
      const salespersonData = await salespersonResponse.json();

      this.setState({ salerecords: saleRecData.salerecords, salespeople: salespersonData.salespeople });
    }
  }


  render() {
    let dropdownClasses = 'form-select d-none';
    if (this.state.salespeople.length > 0) {
      dropdownClasses = 'form-select';
    }
    return (
      <div className="mb-3">
      <select onChange={this.handleChange} required name="salesperson" id="salesperson" className={dropdownClasses} >
        <option value="">Choose a salesperson</option>


      {/* <table className="table table-striped">
        <thead>
          <tr>
            <th>SalesPerson Name</th>
            <th>Customer Name</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {this.state.salerecords.filter(salerecord => salerecord.salesperson.name === this.state.salesperson {
            return (
              <tr key={salerecord.automobile.vin}>
                <td>{ salerecord.salesperson.name }</td>
                <td>{ salerecord.customer.name }</td>
                <td>{ salerecord.automobile.vin }</td>
                <td>{ salerecord.price }</td>
              </tr>
            );
          })}
        </tbody>
      </table> */}
      </select>
      </div>
    );
  }
}

// make a dropdown of all salespeople
// event handle: click on a SP's name
// loop over sale recs. perhaps a filtering function
// if salerecs.salesperson.name matches it
// then add the relevant properties to the table.

// was not able to get the above to work following this pseudocode, table must be outside of select tags.

export default SalesPersonHistoryList;