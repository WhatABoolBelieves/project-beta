import React from 'react';

class SaleRecordForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      automobile: '',
      automobiles: [],
      salesperson: '',
      salespeople: [],
      customer: '',
      customers: [],
      price: '',
      has_sold: false,
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const newState = {};
    newState[event.target.id] = event.target.value;
    this.setState(newState);
  }

  async componentDidMount() {
    const autoUrl = 'http://localhost:8100/api/automobiles/';
    const salespersonUrl = 'http://localhost:8090/api/salespeople/';
    const customerUrl = 'http://localhost:8090/api/customers/';

    const autoResponse = await fetch(autoUrl);
    const salespersonResponse = await fetch(salespersonUrl);
    const customerResponse = await fetch(customerUrl);


    if (autoResponse.ok && salespersonResponse.ok && customerResponse.ok) {
      const autoData = await autoResponse.json();
      const salespersonData = await salespersonResponse.json();
      const customerData = await customerResponse.json();

      this.setState({ automobiles: autoData.automobiles, salespeople: salespersonData.salespeople, customers: customerData.customers });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    data.has_sold = true;

    const autoID = data.automobile;

    const saleRecordUrl = 'http://localhost:8090/api/salerecords/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(saleRecordUrl, fetchConfig);
    if (response.ok) {
      const newSaleRecord = await response.json();
      this.setState({
        automobile: '',
        salesperson: '',
        customer: '',
        price: '',
      });
    }
  }

  render() {
    let dropdownClasses = 'form-select d-none';
    if (this.state.automobiles.length > 0) {
      dropdownClasses = 'form-select';
    }
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a sale record.</h1>
            <form onSubmit={this.handleSubmit} id="create-salerecord-form">

               <div className="mb-3">
                <select onChange={this.handleChange} required name="automobile" id="automobile" className={dropdownClasses} >
                  <option value="">Choose a car</option>
                  {this.state.automobiles.map(automobile => {
                        return (
                          <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                        )
                      })}
                </select>
              </div>

            <div className="mb-3">
                <select onChange={this.handleChange} required name="salesperson" id="salesperson" className={dropdownClasses} >
                  <option value="">Choose a salesperson</option>
                  {this.state.salespeople.map(salesperson => {
                        return (
                          <option key={salesperson.name} value={salesperson.name}>{salesperson.name}</option>
                        )
                      })}
                </select>
              </div>

              <div className="mb-3">
               <select onChange={this.handleChange} required name="customer" id="customer" className={dropdownClasses} >
                  <option value="">Choose a customer</option>
                  {this.state.customers.map(customer => {
                        return (
                          <option key={customer.name} value={customer.name}>{customer.name}</option>
                        )
                      })}
                </select>
              </div>

              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.price} placeholder="Price" type="float" id="price" className="form-control" />
                <label htmlFor="price">Price: </label>
              </div>

              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default SaleRecordForm;

