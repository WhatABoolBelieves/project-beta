import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import SalesPersonForm from './SalesPersonForm';
import CustomerForm from './CustomerForm';
import SaleRecordForm from './SaleRecordForm';
import SaleRecordList from './SaleRecordList';
import SalesPersonHistoryList from './SalesPersonHistory';
import Nav from './Nav';
import AppointmentList from './Services/AppointmentList';
import AppointmentForm from './Services/AppointmentForm';
import AppointmentHistory from './Services/AppointmentHistory'
import TechnicianForm from './Services/TechnicianForm';
import ManufacturerList from './Inventory/ManufacturerList';
import ManufacturerForm from './Inventory/ManufacturerForm';
import ModelList from './Inventory/ModelList';
import ModelForm from './Inventory/ModelForm';
import AutomobileList from './Inventory/AutomobileList';
import AutomobileForm from './Inventory/AutomobileForm';

function App(props) {
  // let [salerecords, setSaleRecords] = useState({})

  // async function fetchSaleRecords() {
  //   const res = await fetch('http://localhost:8090/api/salerecords/');
  //   const newRecsList = await res.json();
  //   setSaleRecords(newRecsList)
  // }

  // useEffect(() => {
  //   fetchSaleRecords()
  // }, [])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          {/* Inventory */}
          <Route path="manufacturers/" element={<ManufacturerList manufacturers={props.manufacturers} />} />
          <Route path="manufacturers/new/" element={<ManufacturerForm manufacturers={props.manufacturers} />} />
          <Route path="models/" element={<ModelList models={props.models} />} />
          <Route path="models/new/" element={<ModelForm models={props.models} />} />
          <Route path="automobiles/" element={<AutomobileList />} />
          <Route path="automobiles/new/" element={<AutomobileForm automobiles={props.automobiles} />} />
          {/* These lists were working before the merge. I thought I understood what I was
           merging - I merged branches successfully before. Now I'm getting error messages
           in the console that each child needs a unique key prop. */}

          {/* Services */}
          <Route path="appointments/" element={<AppointmentList appointments={props.appointments} />} />
          <Route path="appointments/new/" element={<AppointmentForm appointments={props.appointments} />} />
          <Route path="history/" element={<AppointmentHistory appointments={props.appointments} />} />
          <Route path="technicians/new/" element={<TechnicianForm technician={props.technician} />} />

          {/* Sales */}
          <Route path="salespeople/new" element={<SalesPersonForm />} />
          <Route path="customers/new" element={<CustomerForm />} />
          <Route path="salerecords/new" element={<SaleRecordForm />} />
          <Route path="salerecords" element={<SaleRecordList />} />

          <Route path="salesperson/history" element={<SalesPersonHistoryList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
