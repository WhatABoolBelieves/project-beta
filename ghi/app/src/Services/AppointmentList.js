import React from 'react';

class AppointmentList extends React.Component {
    constructor(props) {
      super(props)
      this.state = { 
        appointments: [],
      }
    };
    
    
    async getAppointmentData() {
        const url = 'http://localhost:8080/api/appointments/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ appointments: data.appointments })
            console.log(this.state)
        }
    }

    async componentDidMount() {
        this.getAppointmentData();
    }

    async finishAppointment(event) {
        const href = event.target.value;
        const url = `http://localhost:8080${href}`
        const response = await fetch(url);
        const data = await response.json();
        const appointmentUrl1 = 'http://localhost:8080/api/history/';
        const fetchConfig1 = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
        };
        const response1 = await fetch(appointmentUrl1, fetchConfig1);
        if (response1.ok) {
            const newAppointment = await response1.json();
            console.log(newAppointment);
            this.setState({ 
                vin: '',
                name: '',
                date: '',
                tech: '',
                reason: '',
            });
        }
        const appointmentUrl2 = `http://localhost:8080${href}`
        const fetchConfig2 = {method: "DELETE"};
        const response2 = await fetch(appointmentUrl2, fetchConfig2);
        if (response2.ok) {
            this.getAppointmentData()
        }
    }

    async cancelAppointment(event) {
        const href = event.target.value;
        const appointmentUrl = `http://localhost:8080${href}`
        const fetchConfig = { method: "DELETE" };
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            this.getAppointmentData();
        }
    }

    render() {
        return (
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer Name</th>
                        <th>Date/Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.appointments.map(appointment => {
                        return (
                            <tr>
                                <td>{ appointment.vin }</td>
                                <td>{ appointment.name }</td>
                                <td>{ appointment.date }</td>
                                <td>{ appointment.tech }</td>
                                <td>{ appointment.reason }</td>
                                <td>
                                    <button value={ appointment.href } onClick={ this.cancelAppointment } type="button" className="btn btn-danger">Cancel</button>
                                </td>
                                <td>
                                    <button value={ appointment.href } onClick={ this.finishAppointment } type="button" className="btn btn-danger">Finish</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        )
    }
}

export default AppointmentList;

