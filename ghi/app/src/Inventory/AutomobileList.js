import React from 'react';


class AutomobileList extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
        automobiles: [],
      }
    };

    async getAutomobileData() {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ automobiles: data.automobiles })
            console.log(this.state)
        }
    }

    async componentDidMount() {
        this.getAutomobileData()
    }

    render() {
        return (
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Year</th>
                        <th>Color</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.automobiles.map(automobile => {
                        return (
                            <tr>
                                <td>{ automobile.vin }</td>
                                <td>{ automobile.year }</td>
                                <td>{ automobile.color }</td>
                                <td>{ automobile.model.name }</td>
                                <td>{ automobile.model.manufacturer.name }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        )
    }
}

export default AutomobileList;