import React from 'react';

class AutomobileForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
        automobile: '',
        automobiles: [],
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const newState= {};
    newState[event.target.id] = event.target.value;
    this.setState(newState);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    const automobileUrl = 'http://localhost:8100/api/automobiles/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(automobileUrl, fetchConfig);
    if (response.ok) {
      const newAutomobile = await response.json();
      console.log(newAutomobile);
      this.setState({
        automobile: '',
        automobiles: [],
      });
    }

  }

  render() {
    return(
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add an automobile to inventory</h1>
            <form onSubmit={this.handleSubmit} id="create-automobile-form">

              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.color} placeholder="color" required type="text" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.year} placeholder="year" required type="number" id="year" className="form-control" />
                <label htmlFor="year">Year</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.vin} placeholder="vin" required type="text" id="vin" className="form-control" />
                <label htmlFor="vin">VIN</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.model} placeholder="model" required type="text" id="model" className="form-control" />
                <label htmlFor="model">Model</label>
              </div>

              <button className='btn btn-primary'>Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default AutomobileForm;
