import React from 'react';

class CustomerForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      address: '',
      phone: '',
    }
    // does address need state abbreviation and all that goes with it? It shall be all one string for now.

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const newState = {};
    newState[event.target.id] = event.target.value;
    this.setState(newState);
  }

  // do I need a componentDidMount function? Not yet sure what that component would be here.

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};

    const customerUrl = 'http://localhost:8090/api/customers/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(customerUrl, fetchConfig);
    if (response.ok) {
      // const newCustomer = await response.json();
      // console.log(newCustomer);
      this.setState({
        name: '',
        address: '',
        phone: '',
      });
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new potential customer.</h1>
            <form onSubmit={this.handleSubmit} id="create-customer-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.name} placeholder="Name" type="text" id="name" className="form-control" />
                <label htmlFor="name">Customer name: </label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.address} placeholder="Address" type="text" id="address" className="form-control" />
                <label htmlFor="address">Address: </label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.phone} placeholder="Phone" type="text" id="phone" className="form-control" />
                <label htmlFor="phone">Phone: </label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default CustomerForm;

